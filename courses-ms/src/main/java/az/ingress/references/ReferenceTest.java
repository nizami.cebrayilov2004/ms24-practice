package az.ingress.references;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ReferenceTest {

    public static void main(String[] args) throws InterruptedException {

        List<SoftReference<GarbageCollectorTest>> myList = new ArrayList<>();

        System.out.println("Application started");

//        for (int i = 0; i<Integer.MAX_VALUE; i++){
//            SoftReference<String> softReference = new SoftReference<>("Some string " + ++i);
//            myList.add(softReference);
//            myList.add(new SoftReference<>(new GarbageCollectorTest("Some string " + i)));
//        }
//        System.out.println("Application completed");

//        SoftReference<String> softReference = new SoftReference<>(new String("3 actionable tasks: 2 executed, 1 up-to-date"));
//        WeakReference<String > weakReference = new WeakReference<>(new String("3 actionable tasks: 2 executed, 1 up-to-date"));
//        new GarbageCollectorTest("Test");
//
//        System.out.println("BGC Soft reference " + softReference.get());
//        System.out.println("BGC Weak reference " + weakReference.get());
//        System.gc();
//        Thread.sleep(10_000);
//        System.out.println("AGC Soft reference " + softReference.get());
//        System.out.println("AGC Weak reference " + weakReference.get());
//        ReferenceQueue referenceQueue = new ReferenceQueue<>();
//        PhantomReference<String >phantomReference = new PhantomReference<>(new String("Hello Ingress"), referenceQueue);
//        System.out.println("Phantom reference : " + phantomReference.get());
//
//        System.out.println("Reference queue : " + referenceQueue.poll());
    }


    private static void sayHello(int i){

        System.out.println("Hello World " + i);
        sayHello(++i);
    }
}
