package az.ingress.references;

public class GarbageCollectorTest {
    private String string;

    public GarbageCollectorTest(String string) {
        this.string = string;

    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Object is cleaned up by GC " + string);
    }
}
