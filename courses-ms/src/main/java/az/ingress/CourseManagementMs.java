package az.ingress;

import az.ingress.Service.dto.AccountEntity;
import az.ingress.domain.CourseEntity;
import az.ingress.domain.StudentEntity;
import az.ingress.repo.AccountRepository;
import az.ingress.repo.StudentRepository;
import az.ingress.Service.StudentService;
import az.ingress.Service.TransferService;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
//
@SpringBootApplication
@RequiredArgsConstructor
public class CourseManagementMs implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(CourseManagementMs.class, args);
    }

    private final StudentService studentService;
    private final StudentRepository studentRepository;
    private final AccountRepository accountRepository;
    private final TransferService transferService;
    private EntityManagerFactory entityManagerFactory;


    @Override
    public void run(String... args) throws Exception {
//		for (int i = 0; i < 100; i++) {
//			studentRepository.save(studentEntity());
//				}

//		Thread thread = new Thread(()-> transferService.transfer(1L,2L,200D));
//		Thread thread2 = new Thread(()-> transferService.transfer(2L,1L,200D));
//
//		System.out.println("Transfer started");
//		thread.start();
//		thread2.start();
//		thread2.join();
//		thread.join();
//		System.out.println("Transfer end");
//
//
//
//		AccountEntity sourceAccount =
//				AccountEntity
//						.builder()
//						.owner("Elesker")
//						.balance(500D)
//						.build();
//		AccountEntity targetAccount =
//				AccountEntity
//						.builder()
//						.owner("Jabar")
//						.balance(200D)
//						.build();
//
		transferService.transfer(1L,2L,100D);
//		accountRepository.save(sourceAccount);
//		accountRepository.save(targetAccount);

//	}
//		for (int i = 0; i < 100; i++) {
//			studentRepository.save(studentEntity());
//		}
    }
    //
    private StudentEntity studentEntity(){
        StudentEntity student =
                StudentEntity
                        .builder()
                        .firstName("Nizami")
                        .lastName("Jabrayilov")
                        .build();

        CourseEntity courseEntity =
                CourseEntity
                        .builder()
                        .name("MS 24")
                        .student(student)
                        .build();
        CourseEntity courseEntity2 =
                CourseEntity
                        .builder()
                        .name("OCA")
                        .student(student)
                        .build();
        student.setCourses(List.of(courseEntity2, courseEntity));
        return student;
    }

}

