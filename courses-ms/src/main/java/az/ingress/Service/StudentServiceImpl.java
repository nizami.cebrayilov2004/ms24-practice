package az.ingress.Service;

import az.ingress.Service.dto.StudentDto;
import az.ingress.repo.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{
private final ModelMapper modelMapper;
 private final StudentRepository studentRepository;






    @Override
    public List<StudentDto> listStudent() {
        return studentRepository.findAllJpql()
                .stream()
                .map(studentEntity -> modelMapper.map(studentEntity , StudentDto.class))
                .toList();

    }
//  @Override
//  //@Transactional(rollbackOn = Exception.class, r = true)
//    public void testTransaction() throws Exception {
//
//      actualMethod();
//      String myCoolAnimal = new String("Cat"); //STRONG REFERENCE
//
//      SoftReference<String> softReference = new SoftReference<>("Dog"); // SOFT - OUT OF MEMORY OLANDA SILINIR
//      softReference.get(); //null qayida biler
//
//      WeakReference<String> weakReference = new WeakReference<>("Tiger"); //WEAK REFERENCE
//      weakReference.get(); // GC ISLESE NULL QAYIDIR , ISLEMESE REFERENCE VALUE QAYIDIR
//
//      ReferenceQueue<String> referenceQueue = new ReferenceQueue<>();
//      PhantomReference<String> phantomReference = new PhantomReference<>("Lion", referenceQueue);
//      phantomReference.get(); //NULL qayidir
//


      //      StudentEntity studentEntity =
//              StudentEntity.builder().
//                      firstName("RUstem")
//                      .lastName("Ceferov")
//                      .build();
//      studentRepository.save(studentEntity);
//      if (1==1)
//          throw new Exception("Something failed");
//      studentRepository.deleteById(2L);








//     final StudentEntity studentEntity =  studentRepository
//              .findById(4L)
//              .orElseThrow();
//      //StudentEntity studentEntity = new StudentEntity();
//      studentEntity.setFirstName("Nizami");
//      studentEntity.setLastName("Jabrayilovv");
//     // studentRepository.save(studentEntity);
//      System.out.println("COMPLETED");
//  }

//    @Transactional(rollbackFor = Exception.class, readOnly = true)
//    public void actualMethod() throws Exception {
//      StudentEntity studentEntity =
//              StudentEntity.builder().
//                      firstName("RUstem")
//                      .lastName("Ceferov")
//                      .build();
//      studentRepository.save(studentEntity);
//      if (1==1)
//          throw new Exception("Something failed");
//      studentRepository.deleteById(2L);
//  }

}
