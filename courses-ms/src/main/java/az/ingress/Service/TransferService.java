package az.ingress.Service;

public interface  TransferService {
    void transfer(Long sourceAccountID, Long targetAccountID, Double amount );
}
