package az.ingress.Service.dto;

import lombok.Data;

import java.util.List;

@Data
public class StudentDto {
    private Long id;
    private String name;
    private String surname;
    private List<CourseDto> courses;



}
