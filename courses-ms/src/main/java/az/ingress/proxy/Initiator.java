package az.ingress.proxy;

import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.PushBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Initiator {

    private final AnimalService animalService;
    @PostConstruct
    public void init(){
        //AnimalService animalService = new AnimalServiceImpl2();
        animalService.makeNoise();
    }
}
