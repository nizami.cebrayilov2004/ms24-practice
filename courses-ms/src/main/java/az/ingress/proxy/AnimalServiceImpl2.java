package az.ingress.proxy;

public class AnimalServiceImpl2 extends AnimalServiceImpl1{


    @Override
    public void makeNoise() {
        System.out.println("Begin transaction");
        super.makeNoise();
        System.out.println("Commit transaction");
    }
}
