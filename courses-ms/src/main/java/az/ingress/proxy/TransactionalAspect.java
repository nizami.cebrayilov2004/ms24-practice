package az.ingress.proxy;

import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
@Aspect
@Component

public class TransactionalAspect {

//    @Pointcut("execution(public * az.ingress.proxy.*.makeNoise(..))")
//    private void publicMethodsFromLoggingPackage(){
//
//    }
//
//    @Before("execution(public * az.ingress.proxy.*.makeNoise(..))")
//    public void methodBeforeAdvice(){
//        System.out.println("Animal will bark now");
//    }

//    @Before("execution(* *..makeNoise(..))")
//    public void methodBeforeAdvice(){
//        System.out.println("Animal will bark now");
//    }

    @SneakyThrows
    @Around("@annotation(az.ingress.proxy.LogExecutionTime)")
    public void methodBeforeAdvice(ProceedingJoinPoint proceedingJoinPoint){
        System.out.println("Animal will bark now");
        proceedingJoinPoint.proceed();
        System.out.println("Animal stopped barking");
    }
}
