package az.ingress.repo;
import az.ingress.domain.StudentEntity;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity,Long> {
   @Lock(LockModeType.OPTIMISTIC)
    @EntityGraph(attributePaths = "courses")
    List<StudentEntity> findAll();

//    List<StudentEntity> findAllNativeQuery();

    @Query("From StudentEntity s JOIN FETCH s.courses c")
    List<StudentEntity> findAllJpql();





}
