package az.ingress.auth.jwt;

import az.ingress.auth.jwt.config.JwtTokenConfigProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.security.Key;

@Service
@RequiredArgsConstructor
public class JwtService {

    private final JwtTokenConfigProperties properties;
    private Key key;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        if (StringUtils.isBlank(properties.getJwtProperties().getSecret())) {
            throw new RuntimeException("Token config not found");
        }
        keyBytes = Decoders.BASE64.decode(properties.getJwtProperties().getSecret());
        key = Keys.hmacShaKeyFor(keyBytes);
    }


    public String issueToken(Authentication authentication) {
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getName())
                .claim("authorities", authentication
                        .getAuthorities()
                        .stream()
                        .map(a -> a.getAuthority())
                        .toList()
                )
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration
                        .ofSeconds(properties.getJwtProperties()
                                .getTokenValidityInSeconds()))))
                .signWith(key, SignatureAlgorithm.HS256);
        return jwtBuilder.compact();
    }


    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }
}
