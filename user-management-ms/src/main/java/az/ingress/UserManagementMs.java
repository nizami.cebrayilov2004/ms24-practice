package az.ingress;

import az.ingress.auth.jwt.JwtService;
import az.ingress.domain.UserEntity;
import az.ingress.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class UserManagementMs implements CommandLineRunner {

    private final UserRepository userRepository;
    private final JwtService jwtService;
    public static void main(String[] args) {
        SpringApplication.run(UserManagementMs.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        UserEntity userEntity
                = UserEntity.builder()
                .username("user")
                // .authorities(List.of())
                .password("{noop}1234")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .enabled(true)
                .credentialsNonExpired(true)
                .build();

        userRepository.save(userEntity);
    }


}