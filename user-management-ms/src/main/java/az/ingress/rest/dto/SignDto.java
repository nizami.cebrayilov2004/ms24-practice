package az.ingress.rest.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "password")
public class SignDto {

    private String username;
    private String password;
}
