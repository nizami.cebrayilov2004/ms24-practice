package az.ingress.rest;

import az.ingress.auth.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class SecurityApi  {
private final String BEARER = "bearer";
    private final JwtService jwtService;
    @GetMapping("/public")
    public String sayHello(@RequestHeader("authorization") String authorizationHeader){
       final String jwt = authorizationHeader.substring(BEARER.length())
                .trim();
        System.out.println(jwt);
        return "Hello world" + jwtService.parseToken(jwt);
    }

    @GetMapping("/authenticated")
    public String sayHelloAuthenticated(Authentication authentication) {
        return jwtService.issueToken(authentication);
    }



    @GetMapping("/role-user")
    public String sayHelloRoleUser(){
        return "Hello world role-user";
    }

    @GetMapping("/role-admin")
    public String sayHelloRoleAdmin(){
        return "Hello world role-admin";
    }
}
