package az.ingress.repo;

import az.ingress.domain.UserEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
   @EntityGraph(attributePaths = "authorities")
    Optional findByUsername(String username);
}
