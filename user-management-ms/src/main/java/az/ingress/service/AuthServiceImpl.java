package az.ingress.service;

import az.ingress.auth.jwt.JwtService;
import az.ingress.auth.jwt.config.JwtTokenConfigProperties;
import az.ingress.exception.UnAuthorizedException;
import az.ingress.domain.RefreshTokenEntity;
import az.ingress.domain.UserEntity;
import az.ingress.repo.RefreshTokenRepository;
import az.ingress.repo.UserRepository;
import az.ingress.rest.dto.JwtAccessToken;
import az.ingress.rest.dto.RefreshToken;
import az.ingress.rest.dto.SignDto;
import az.ingress.rest.dto.SignInResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final JwtTokenConfigProperties properties;
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final RefreshTokenRepository refreshTokenRepository;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;

    @Override
    public SignInResponse signIn(SignDto signDto) {
        Authentication token = new UsernamePasswordAuthenticationToken(signDto.getUsername(), signDto.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(token);
        final String jwtToken = jwtService.issueToken(authentication);
        return SignInResponse.builder()
                .accessToken(JwtAccessToken.builder()
                        .token(jwtToken)
                        .build())
                        .refreshToken(createRefreshToken(signDto.getUsername()))
                        .build();
    }

    @Override
    public void signOut(String refreshTokenStr) {
        final Optional<RefreshTokenEntity> refreshToken = refreshTokenRepository.findByToken(refreshTokenStr);
        if (refreshToken.isEmpty()) {
            return;
        }

        final RefreshTokenEntity refreshTokenEntity = refreshToken.get();
        refreshTokenEntity.setValid(false);
        refreshTokenRepository.save(refreshTokenEntity);
    }

    @Override
    public SignInResponse refreshToken(String refreshTokenStr) {
        final Optional<RefreshTokenEntity> refreshTokenOptional = refreshTokenRepository.findByToken(refreshTokenStr);
        checkIfExists(refreshTokenOptional);
        RefreshTokenEntity refreshToken = refreshTokenOptional.get();
        checkIfValid(refreshToken);
        final Optional<UserEntity> optionalUserEntity = userRepository.findByUsername(refreshToken.getUsername());
        checkIfUserExists(optionalUserEntity);
        final UserEntity userEntity = optionalUserEntity.get();
        checkIfUserAccountIsActive(userEntity);
        final RefreshToken newRefreshToken = createRefreshToken(refreshToken.getUsername());
        endOldToken(refreshToken);
        Authentication authentication = new UsernamePasswordAuthenticationToken(refreshToken.getUsername(), "", userEntity.getAuthorities());
        final String jwtToken = jwtService.issueToken(authentication);
        return SignInResponse.builder()
                .accessToken(JwtAccessToken.builder()
                        .token(jwtToken)
                        .build())
                .refreshToken(newRefreshToken)
                .build();
    }

    private void endOldToken(RefreshTokenEntity refreshToken) {
        refreshToken.setValid(false);
        refreshTokenRepository.save(refreshToken);
    }

    private void checkIfUserAccountIsActive(UserEntity userEntity) {
        if (userEntity.isAccountNonExpired()
                && userEntity.isAccountNonLocked()
                && userEntity.isCredentialsNonExpired()
                && userEntity.isEnabled()) {
            return;
        }
        throw new UnAuthorizedException("User account is not active");
    }

    private void checkIfUserExists(Optional<UserEntity> optionalUserEntity) {
        if (optionalUserEntity.isEmpty()) {
            throw new UnAuthorizedException("User not found");
        }
    }

    private void checkIfExists(Optional<RefreshTokenEntity> refreshTokenOptional) {
        if (refreshTokenOptional.isEmpty()) {
            throw new UnAuthorizedException("Token not found");
        }
    }

    private void checkIfValid(RefreshTokenEntity refreshToken) {
        if (!refreshToken.getValid() || refreshToken.getEat()
                .toInstant().isBefore(Instant.now())) {
            throw new UnAuthorizedException("Token is expired");
        }
    }

    private RefreshToken createRefreshToken(String username) {
        RefreshTokenEntity refreshTokenEntity =
                RefreshTokenEntity
                        .builder()
                        .iat(new Date())
                        .eat(Date.from(Instant
                                .now()
                                .plusSeconds(properties
                                        .getJwtProperties()
                                        .getRefreshTokenValidityInSeconds())))
                        .valid(true)
                        .username(username)
                        .token(UUID.randomUUID().toString())
                        .build();
        refreshTokenRepository.save(refreshTokenEntity);
        return modelMapper.map(refreshTokenEntity, RefreshToken.class);
    }
}