package az.ingress.service;

import az.ingress.rest.dto.SignDto;
import az.ingress.rest.dto.SignInResponse;

public interface AuthService {
    SignInResponse signIn(SignDto signDto);

    void signOut(String refreshTokenStr);

    SignInResponse refreshToken(String refreshTokenStr);
}