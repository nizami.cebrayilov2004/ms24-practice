package az.ingress.concurrency;

import lombok.SneakyThrows;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingQueueExample {
    BlockingQueue<String> tasks = new LinkedBlockingQueue<>(5);

    @SneakyThrows
    public static void main(String[] args) {
        BlockingQueueExample example = new BlockingQueueExample();
        ExecutorService senders = Executors.newFixedThreadPool(3);

        ExecutorService printer = Executors.newFixedThreadPool(2);

        System.out.println("Printers started: ");
        for (int i = 0; i < 2; i++) {
            printer.submit(() -> example.print());
        }
        Thread.sleep(5000);

        System.out.println("Senders started: ");
        for (int i = 0; i < 3; i++) {
            senders.submit(example::send);
        }
    }

    @SneakyThrows
    public void send() {
        while (true) {
            Random random = new Random();
         //   long i = random.nextInt(100, 5000);
           // System.out.println("sleep time: " + i);
            long message = random.nextLong();
            System.out.println("Adding message " + message);
            tasks.add("message " + message);
            System.out.println("message adding done " + message);
        }
    }

    @SneakyThrows
    public void print() {
        while (true) {
            Thread.sleep(100);
           // final String message = tasks.take();
           // System.out.println("Message is " + message + " " + Thread.currentThread().getName());
        }

    }
}
