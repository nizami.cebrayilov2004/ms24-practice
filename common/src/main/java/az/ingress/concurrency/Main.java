package az.ingress.concurrency;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static StringBuffer message = new StringBuffer("hello");

    public static void main(String[] args) throws InterruptedException {
//        Thread thread = new Thread(() -> Main.modify(0, 1000));
//        Thread thread1 = new Thread(() -> Main.modify(1000, 2000));
//        thread.start();
//        thread1.start();
//
//        thread.join();
//        thread1.join();
//        System.out.println(message);

        Animal animal = new Animal(
                1L,
                "tomcat",
                20, new StringBuilder("This is a hero"),
                new ArrayList<>(List.of(new StringBuilder("Dog"))));

        System.out.println("Initial : " + animal);
        animal.getDetails().append("hello");
        animal.getFriends().add(new StringBuilder("TEST"));

        System.out.println("Current : " + animal );
    }

    private static void modify(int from, int to) {
        for (int i = from; i < to; i++) {
            message = message.append(" " + i);
        }
    }
}
@Getter
@ToString
final class Animal {
   private long id;
   private String name;
   private int age;
   private StringBuilder details;
   private List<StringBuilder>friends;

    public Animal(long id, String name, int age, StringBuilder details, List<StringBuilder> friends) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.details = details;
        this.friends = friends;
    }

    public StringBuilder getDetails(){
        return new StringBuilder(details);
    }

    public List<StringBuilder> getFriends(){
        List<StringBuilder> list = friends.stream().map(friends -> new StringBuilder()).toList();
        return new ArrayList<>(list);
    }
}

/* Thread -> call stack
local primitive variable, local reference variable
qalanlari heapde
*/
