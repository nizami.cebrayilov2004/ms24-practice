package az.ingress.concurrency;

import lombok.SneakyThrows;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SemaphoreExample {

    private Semaphore semaphore = new Semaphore(7);
    private Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        SemaphoreExample semaphoreExample = new SemaphoreExample();
        Runnable taskA = () -> semaphoreExample.test();
        ExecutorService executorService = Executors.newFixedThreadPool(100);

        for (int i = 0; i < 100; i++) {
            executorService.submit(taskA);
        }
    }

    @SneakyThrows
    public void test() {
        //lock.lock();
        semaphore.acquire();
        System.out.println("Permits: " + semaphore.availablePermits());
        System.out.println("Thread is here : " + Thread.currentThread().getName());
        Thread.sleep(100);
        System.out.println("DONE " + Thread.currentThread().getName());
        //lock.unlock();
        semaphore.release();
    }

}
