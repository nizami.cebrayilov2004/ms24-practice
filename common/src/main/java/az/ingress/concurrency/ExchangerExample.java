package az.ingress.concurrency;

import ch.qos.logback.core.joran.conditional.ThenAction;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Exchanger;

public class ExchangerExample {
    public static void main(String[] args) {
        Exchanger<String> exchanger = new Exchanger<>();

        Runnable taskA = () -> {
            try {
                Thread.sleep(200);
                String message = exchanger.exchange("message from A");
                System.out.println("Task A: " + message);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };

        Runnable taskB = () -> {
            try {
                Thread.sleep(100);
                String message = exchanger.exchange("message from B");
                System.out.println(("Task B: " + message));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };

        CompletableFuture
                .allOf(CompletableFuture.runAsync(taskA),
                        CompletableFuture.runAsync(taskB))
                .join();
        System.out.println("DONE");
    }
}
