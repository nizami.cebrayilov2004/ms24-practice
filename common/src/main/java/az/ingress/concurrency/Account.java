package az.ingress.concurrency;

public class Account {
    private int balance;

    public Account(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }

    public synchronized void withdraw(int amount) {
        while (amount > balance) {
            System.out.println(Thread.currentThread().getName() + " Amount is greater than current balance, waiting... " + amount);
            try {
                wait();
                System.out.println("Notifying continued");
            } catch (InterruptedException exception) {
            }
        }
            balance = balance - amount;
        }

        public synchronized void deposit ( int amount){
            System.out.println(Thread.currentThread().getName() + " Depositing amount " + amount);
            balance = balance + amount;
            System.out.println("Depositing completed");
            notify();
        }

}