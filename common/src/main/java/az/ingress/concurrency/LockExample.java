package az.ingress.concurrency;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockExample {
    Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        LockExample lockExample = new LockExample();
        CompletableFuture.allOf(CompletableFuture.runAsync(lockExample::test)).join();
        System.out.println("DONE");
    }

    public void test() {
        System.out.println("Message from test without lock ");
        lock.lock();
        lock.lock();
        System.out.println("Message from test with lock ");
        test1();
        System.out.println("Message from test without lock ");
    }

    public void test1() {
        System.out.println("Message from test1 with lock ");
        lock.unlock();
        System.out.println("Message from test1 without lock ");

    }
}
