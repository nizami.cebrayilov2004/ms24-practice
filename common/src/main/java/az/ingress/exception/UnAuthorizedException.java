
package az.ingress.exception;

public class UnAuthorizedException extends RuntimeException {

    public UnAuthorizedException(Throwable throwable) {
        super(throwable);
    }

    public UnAuthorizedException(String message) {
        super(message);
    }
}